const express = require('express');
const { join } = require('path');
const app = express();
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const authConfig = require('./auth_config.json');

// Token auth for API
const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: 'https://dev-xqvk8v1n.auth0.com/.well-known/jwks.json'
  }),
  audience: 'order',
  issuer: 'https://dev-xqvk8v1n.auth0.com/',
  algorithms: ['RS256']
});

// API endpoint for "fake" pizza order
app.get('/api/order', checkJwt, (req, res) => {
  res.send({
    msg: 'Your access token was successfully validated!'
  });
});

// Serve static assets from the /public folder
app.use(express.static(join(__dirname, 'public')));

// Endpoint to serve the configuration file
app.get('/auth_config.json', (req, res) => {
  res.sendFile(join(__dirname, 'auth_config.json'));
});

// Serve the index page for all other requests
app.get('/*', (_, res) => {
  res.sendFile(join(__dirname, 'index.html'));
});

app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    return res.status(401).send({ msg: 'Invalid token' });
  }

  next(err, req, res);
});

// Port setup
app.listen(process.env.PORT || 3000, () =>
  console.log('Application running on port 3000')
);
