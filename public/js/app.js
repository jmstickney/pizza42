let auth0 = null;

// Fetch auth config
const fetchAuthConfig = () => fetch('/auth_config.json');

// Retrieve config and init
const configureClient = async () => {
  const response = await fetchAuthConfig();
  const config = await response.json();

  auth0 = await createAuth0Client({
    domain: config.domain,
    client_id: config.clientId,
    audience: config.audience,
    scope: 'https://www.googleapis.com/auth/contacts.readonly' // attempting to pass scopes here to avoid consent issues
  });
};

// Initialize the application onload
window.onload = async () => {
  await configureClient();

  updateUI();

  const isAuthenticated = await auth0.isAuthenticated();

  const query = window.location.search;
  if (query.includes('code=') && query.includes('state=')) {
    // Process the login state
    await auth0.handleRedirectCallback();

    updateUI();

    // Use replaceState to redirect the user away and remove the querystring parameters
    window.history.replaceState({}, document.title, '/');
  }
};

// Display / hide buttons based on auth
const updateUI = async () => {
  const isAuthenticated = await auth0.isAuthenticated();

  if (isAuthenticated) {
    const user = await auth0.getUser();
    document.getElementById('welcome').innerHTML = 'Welcome ' + user.nickname;
    console.log('Welcome ' + user.nickname);
  }

  document.getElementById('btn-logout').disabled = !isAuthenticated;
  document.getElementById('btn-login').disabled = isAuthenticated;
  document.getElementById('btn-call-api').disabled = !isAuthenticated;
};

const login = async () => {
  await auth0.loginWithRedirect({
    redirect_uri: window.location.origin
  });
};

const logout = () => {
  auth0.logout({
    returnTo: window.location.origin
  });
};

// Call the order pizza API endpoint with auth
// If the user hasn't verified their email, don't allow an order
const callApi = async () => {
  const user = await auth0.getUser();
  console.log(user.user_metadata);
  // Has the user verified their email?
  const isEmailVer = user.email_verified;
  if (isEmailVer) {
    console.log('email verified');
    try {
      // Get the access token from the Auth0 client
      const token = await auth0.getTokenSilently();

      // Make the call to the API, setting the token
      // in the Authorization header
      const response = await fetch('/api/order', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });

      // Fetch the JSON result
      const responseData = await response.json(); // not using.  hardcoding pizza ordered

      // Display the result in the output element
      const responseElement = document.getElementById('api-call-result');

      responseElement.innerText = 'Pizza successfully ordered!';
    } catch (e) {
      // Display errors in the console
      console.error(e);
    }
  } else {
    // User has not verified their email
    const responseElement = document.getElementById('api-call-result');
    responseElement.innerText =
      "WOOPS! The order didn't go through.  You must verify your email prior to ordering.  Please check your inbox for the confirmation email.  Once you have verified your email, try again.";
    console.log('email not verified');
  }
};
